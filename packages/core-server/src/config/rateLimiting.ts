export default () => ({
    global: false, // default true
    // max: 15, // default 1000
    // ban: 5, // default null
    timeWindow: 5000, // default 1000 * 60
    cache: 10000, // default 5000
    // allowList: ['127.0.0.1'], // default []
    // redis: new Redis({ host: '127.0.0.1' }), // default null
    skipOnError: true, // default false
    // keyGenerator: function (req) {
    //   /* ... */
    // }, // default (req) => req.raw.ip
    // errorResponseBuilder: function (req, context) {
    //   /* ... */
    // },
    enableDraftSpec: true, // default false. Uses IEFT draft header standard
    addHeadersOnExceeding: {
        // default show all the response headers when rate limit is not reached
        'x-ratelimit-limit': true,
        'x-ratelimit-remaining': true,
        'x-ratelimit-reset': true,
    },
    addHeaders: {
        // default show all the response headers when rate limit is reached
        'x-ratelimit-limit': true,
        'x-ratelimit-remaining': true,
        'x-ratelimit-reset': true,
        'retry-after': true,
    },
});

import { randomUUID } from 'crypto';
import {
    FastifyServerOptions,
    ProtoAction,
    ConstructorAction,
} from 'fastify';

const prettyPrintTimeFormat = {
    translateTime: 'HH:MM:ss Z',
    ignore: 'pid,hostname',
};

export default () =>
    ({
        http2: false,
        https: null,
        connectionTimeout: 12000,
        keepAliveTimeout: 10000,
        maxRequestsPerSocket: 0,
        requestTimeout: 10000, // DoS attacks.
        ignoreTrailingSlash: false,
        maxParamLength: 100, // DoS attacks.
        bodyLimit: 1048576, // 1Mb
        onProtoPoisoning: <ProtoAction>'error', // prototype poisoning attacks.
        onConstructorPoisoning: <ConstructorAction>'error', // prototype poisoning attacks.
        logger: {
            prettyPrint:
                process.env.NODE_ENV === 'dev'
                    ? prettyPrintTimeFormat
                    : false,
            level: process.env.LOGGING_LEVEL || 'info',
            redact: ['req.headers.authorization'],
        },
        disableRequestLogging: true,
        jsonShorthand: true, // TODO
        caseSensitive: true,
        requestIdHeader: 'x-request-id',
        requestIdLogLabel: 'x-request-id',
        genReqId: () => {
            return randomUUID();
        },
        trustProxy: false, // only if we implemented a proxy
        pluginTimeout: 10000,
        exposeHeadRoutes: false,
        return503OnClosing: true,
        http2SessionTimeout: 10000,
    } as FastifyServerOptions);

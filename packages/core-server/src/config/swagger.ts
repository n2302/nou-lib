export default () => ({
    routePrefix: '/documentation',
    swagger: {
        info: {
            title: 'Fastify-core Documentation',
            description: 'Fastify swagger API',
            version: '0.1.0',
        },
        externalDocs: {
            url: 'https://swagger.io',
            description: 'Find more info here',
        },
        host: 'localhost',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json'],
    },
    uiConfig: {
        docExpansion: 'no',
        deepLinking: false,
    },
    exposeRoute: true,
});

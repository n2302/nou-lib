export default () => ({
    noCache: true,
    contentSecurityPolicy: {
        directives: {
            defaultSrc: ["'self'"],
            styleSrc: ["'self'", "'unsafe-inline'"],
            imgSrc: ["'self'", "'data:'", "'validator.swagger.io'"],
            scriptSrc: ["'self'", "https: 'unsafe-inline'"],
        },
    },
    referrerPolicy: {
        policy: 'no-referrer',
    },
    hsts: {
        maxAge: 31536000,
    },
});

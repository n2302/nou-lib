export default () => ({
    host: <string>process.env.SERVER_HOST,
    port: <number>Number(<string>process.env.SERVER_PORT),
    isProd:
        <boolean>Boolean(process.env.IS_PROD) &&
        <string>process.env.NODE_ENV === 'production',
    baseUrl: <string>process.env.BASE_URL,
});

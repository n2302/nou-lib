This folder should contain code that is not
dependent on external features
This code should be autonomous and give the app
all it's basic functionalities


All core functionalities loop over each folder feature and looks for its responsability
so we have a contract for adding features and not for what to know from core... the goal is to not care about the core but just concern about the feature and if we delete the feature the core doesnt care.


 Fastify |                    |
         | Bridge or Adapter  |
         |        DB          |
         | Single or Multiple |  PSQL || MONGO 




1. Composability -> this means that I am able to compose the service modularly... I can add API features
2. Reusability -> This meas that I can reuse the same code for different use cases 
import fp from 'fastify-plugin';
import cors from 'fastify-cors';
import { FastifyInstance } from 'fastify';

export default fp(async (fastifyInstance: FastifyInstance) => {
    try {
        fastifyInstance.register(cors);
        fastifyInstance.log.info(
            'CORS has successfully been registered',
        );
    } catch (error) {
        fastifyInstance.log.error(error);
    }
});

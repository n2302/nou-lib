import fp from 'fastify-plugin';
import helmet from 'fastify-helmet';
import { FastifyInstance } from 'fastify';

import { helmet as helmetC } from '../config';

export default fp(async (fastifyInstance: FastifyInstance) => {
    try {
        fastifyInstance.register(helmet, helmetC);
        fastifyInstance.log.info(
            'HELMET has successfully been registered',
        );
    } catch (error) {
        fastifyInstance.log.error(error);
    }
});

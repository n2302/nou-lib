import { FastifyInstance } from 'fastify';
import fp from 'fastify-plugin';
import rateLimit from 'fastify-rate-limit';

import { rateLimiting } from '../config';

export default fp(async (fastifyInstance: FastifyInstance) => {
    try {
        fastifyInstance.register(rateLimit, rateLimiting);
        fastifyInstance.log.info(
            'RATE_LIMITING has successfully been registered',
        );
    } catch (error) {
        fastifyInstance.log.error(error);
    }
});

import { resolve } from 'path';
import { config } from 'dotenv';
import { envSchema } from 'env-schema';
import { IncomingMessage, Server, ServerResponse } from 'http';
import Fastify, {
    FastifyInstance,
    FastifyLoggerInstance,
    FastifyPluginCallback,
    FastifyServerOptions,
} from 'fastify';

import { fastify } from './config';
import {
    SIGNAL_SIGINT,
    SIGNAL_SIGTERM,
    FST_ON_RESPONSE,
    FST_ON_REQUEST,
    EXIT_CODE,
    SIGNAL_UNHANDLED_REJECTION,
} from './common/constants';

import { onRequest, onResponse } from './hooks';
import { coreEnvSchema } from './common/schema';
import { cors, helmet, rateLimiting } from './plugins';

export class FastifyWrapper {
    public server: FastifyInstance<
        Server,
        IncomingMessage,
        ServerResponse,
        FastifyLoggerInstance
    >;

    private hasclosed = false;

    constructor(protected fstOptions?: FastifyServerOptions) {
        this.server = Fastify(
            fstOptions ? { ...fastify(), ...fstOptions } : fastify(),
        );

        this.server.register(cors);
        this.server.register(helmet);
        this.server.register(rateLimiting);

        this.server.addHook(FST_ON_REQUEST, onRequest);
        this.server.addHook(FST_ON_RESPONSE, onResponse);

        this.close = this.close.bind(this);

        process.once(SIGNAL_SIGINT, this.close);
        process.once(SIGNAL_SIGTERM, this.close);
        process.once(SIGNAL_UNHANDLED_REJECTION, this.close);
    }

    /* ----------  Build Methods  ----------*/

    public static async build(fstOptions?: FastifyServerOptions) {
        return new FastifyWrapper(fstOptions);
    }

    public static preload(
        envpath: string,
        envschema?: Record<string, unknown>,
    ) {
        const result = config({
            path: resolve(envpath),
        });

        if (result.error) throw result.error;

        envSchema({
            data: result.parsed,
            schema: {
                type: 'object',
                properties: { ...coreEnvSchema, ...envschema },
            },
        });
    }

    /* ----------  Server Methods  ----------*/

    async ready() {
        try {
            await this.server.ready();

            return this;
        } catch (error) {
            this.server.log.error(error);
            return this.close();
        }
    }

    async listen(
        port: number,
        host: string,
    ): Promise<FastifyWrapper | void> {
        try {
            this.server.listen(port, host);

            return this;
        } catch (error) {
            this.server.log.error(error);
            return this.close();
        }
    }

    async close(signal?: string): Promise<void> {
        if (this.hasclosed) return;

        try {
            this.hasclosed = true;

            if (signal)
                this.server.log.info(`Signal ${signal} received`);

            this.server.log.info('Terminating service...');

            await this.server.close();
        } catch (error) {
            this.server.log.error(error);
            process.exit(EXIT_CODE);
        }
    }

    async register(
        plugin: FastifyPluginCallback<any, Server>,
        opts?: any,
    ): Promise<FastifyWrapper | void> {
        try {
            await this.server.register(plugin, opts);

            return this;
        } catch (error) {
            this.server.log.error(error);
            return this.close();
        }
    }

    async printRoutes() {
        this.server.log.info(this.server.printRoutes());

        return this;
    }
}

export default FastifyWrapper;

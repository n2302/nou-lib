const TS = '(?! )'; // Trailing Spaces
const LS = '(?<! )'; // Leading Spaces
const ALPHABET = 'a-zA-Z';
const TILTS = 'áéíñóúüÁÉÍÑÓÚÜ';
const SPACES = '\\s';
const POSITIVE_NUMBERS = '\\d';
const SPECIAL_CHAR = ',\\-_\\.:';
const EMAIL = {
    PREFIX: `${SPACES}*(?![._-])(?!.*\\.@)(?!.*?\\.\\.)[a-zA-Z0-9_.-]+`,
    SECOND_LEVEL_DOMAIN: `(?![._-])(?!.*[-\\_\\.]{2})[a-zA-Z0-9_.-]+`,
    TOP_LEVEL_DOMAIN: `+[a-zA-Z]{2,20}${SPACES}*$`,
};

export const PATTERNS = {
    NUMBERS: '^[0-9]*$',
    ALPHABET_NUMBER: `[0-9A-K]{1}`,
    NON_EMPTY_STRING: '^(?!\\s*$).+',
    PASSWORD:
        '^(?!.*[\\s])(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[\\W_0-9a-zA-Z!@#$%^&áéíñóúüÁÉÍÑÓÚÜ*]{8,101}$',
    EMAIL: `^${EMAIL.PREFIX}@${EMAIL.SECOND_LEVEL_DOMAIN}\\.${EMAIL.TOP_LEVEL_DOMAIN}$`,
    DATE_OF_BIRTH: '^\\d{4}-\\d{2}-\\d{2}$',
    RUT: '^(\\d{1,3}(?:\\d{1,3}){2})-[\\dK]$',
    ALPHABET: `^${TS}[${ALPHABET}]*${LS}$`,
    DATEOFBIRTH: '^\\d{4}-\\d{2}-\\d{2}$',
    ALPHABET_NUMBERS: `^${TS}[${ALPHABET}${POSITIVE_NUMBERS}]*${LS}$`,
    ALPHABET_NUMBERS_SPACES_SPECIALCHARACTERS: `^${TS}[${ALPHABET}${POSITIVE_NUMBERS}${SPACES}${SPECIAL_CHAR}]*${LS}$`,
    ALPHABET_NUMBERS_SPECIALCHARACTERS: `^${TS}[${ALPHABET}${POSITIVE_NUMBERS}${SPECIAL_CHAR}]*${LS}$`,
    ALPHABET_TILTS_SPACES: `${SPACES}*^[${ALPHABET}${TILTS}${SPACES}]*${SPACES}*$`,
    ALPHABET_TILTS_NUMBERS_SPACES: `^${TS}[${ALPHABET}${TILTS}${SPACES}${POSITIVE_NUMBERS}]*${LS}$`,
    POSITIVE_NUMBERS: `^${TS}[${POSITIVE_NUMBERS}]*${LS}$`,
    VERSION: '^(\\d+\\.)?(\\d+\\.)?(\\*|\\d+)$',
    ENDPOINT: '^[a-zA-Z\\d\\/-]*$',
    INSECURE_SYMBOLS: ['<', '>', '%', ':', ';', '\\\\', '"'],
    FULL_INSECURE_SYMBOLS: [
        '(',
        ')',
        '<',
        '>',
        '%',
        '&',
        '+',
        '/',
        ':',
        ';',
        '\\\\',
        '"',
    ],
    AT: '.+@.+',
};

export default PATTERNS;

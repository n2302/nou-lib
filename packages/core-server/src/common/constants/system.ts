export const SIGNAL_SIGINT = 'SIGINT';
export const SIGNAL_SIGTERM = 'SIGINT';
export const SIGNAL_UNHANDLED_REJECTION = 'unhandledRejection';

export const EXIT_CODE = 1;

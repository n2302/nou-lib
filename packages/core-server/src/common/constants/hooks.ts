export const FST_ON_REQUEST = 'onRequest';
export const FST_PRE_PARSING = 'preParsing';
export const FST_PRE_VALIDATION = 'preValidation';
export const FST_PRE_HANDLER = 'preHandler';
export const FST_PRE_SERIALIZATION = 'preSerialization';
export const FST_ON_ERROR = 'onError';
export const FST_ON_SEND = 'onSend';
export const FST_ON_RESPONSE = 'onResponse';
export const FST_ON_TIMEOUT = 'onTimeout';

export type fastifyHooks =
    | typeof FST_ON_REQUEST
    | typeof FST_PRE_PARSING
    | typeof FST_PRE_VALIDATION
    | typeof FST_PRE_HANDLER
    | typeof FST_PRE_SERIALIZATION
    | typeof FST_ON_ERROR
    | typeof FST_ON_SEND
    | typeof FST_ON_RESPONSE
    | typeof FST_ON_TIMEOUT;

/* ----------  Request Headers  ----------*/

export const REMOTE_HOST_REF = 'X-rhsRef'.toLowerCase();
export const CHANNEL_REF = 'X-chRef'.toLowerCase();
export const COUNTRY_REF = 'X-country'.toLowerCase();
export const SYSTEM_VERSION_REF =
    'X-systemVersionDevice'.toLowerCase();
export const SYSTEM_DEVICE_REF = 'X-systemDevice'.toLowerCase();
export const DATE_TIME_REF = 'X-cmDateTime'.toLowerCase();
export const REQUEST_ENVIRONMENT = 'X-environment'.toLowerCase();
export const CONFIRM_EMAIL = 'x-confirm-email';

/* ----------  Required Headers  ----------*/

export const REQUIRED_HEADERS = [
    REMOTE_HOST_REF,
    CHANNEL_REF,
    COUNTRY_REF,
    SYSTEM_VERSION_REF,
    SYSTEM_DEVICE_REF,
    DATE_TIME_REF,
    REQUEST_ENVIRONMENT,
];

/* ----------  Trace Headers  ----------*/

export const REQUEST_ID = 'x-request-id';
export const TRACE_ID = 'x-b3-traceid';
export const SPAN_ID = 'x-b3-spanid';
export const PARENT_SPAN_ID = 'x-b3-parentspanid';
export const B3_SAMPLED = 'x-b3-sampled';
export const B3_FLAGS = 'x-b3-flags';
export const SPAN_CONTEXT = 'x-ot-span-context';

/* ----------  Methods  ----------*/

export const POST = 'POST';
export const GET = 'GET';
export const PUT = 'PUT';
export const PATCH = 'PATCH';
export const DELETE = 'DELETE';
export const OPTION = 'OPTION';

/* ----------  Urls  ----------*/

export const SERVER_VERSION_PREFIX = '/v1';
export const SERVER_ROOT_ROUTE = '/';
// export const ACCOUNTS_ROUTE = '/account';

/* ----------  Env  ----------*/

export const ENV_PRODUCTION = 'production';
export const ENV_DEVELOPMENT = 'dev';
export const ENV_STAGING = 'staging';

export const BCRYPT = 'bcrypt';

export const ENV = 'env';
export const CONFIGURATION = 'config';
export const PLUGINS = 'plugins';

import { envSchema } from 'env-schema';

export default (schema: Record<string, any>) =>
    envSchema({
        schema,
        dotenv: {
            path: `.env.${process.env.NODE_ENV}`,
        },
    });

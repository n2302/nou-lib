import { ForbiddenError } from './CoreErrors';
import { CustomError } from './CustomError';
import { getPropertyPath } from './utils';
import { ErrorHandler } from './types';
import { AjvError } from '.';

const customErrorHandler: ErrorHandler<CustomError> = error => {
    if (error instanceof CustomError) {
        return error;
    }

    return null;
};

const validationErrorHandler: ErrorHandler<AjvError> = error => {
    if (error.type && error.type === 'CUSTOM_AJV_VALIDATION_ERROR') {
        const errors = error.errors.map((val: any) => {
            const property = getPropertyPath(val);
            const message = `${val.message}`;
            return {
                property,
                message,
                code: 'VALIDATION_ERROR',
            };
        });
        const err = new CustomError(400, '', '', errors);

        return err;
    }

    // Validation phase
    if (error.validation) {
        const errorPath = error.message.split(' ')[0].split('/')[0];
        const errors = error.validation.map((val: any) => {
            const property = getPropertyPath(val);
            let message = `${errorPath} ${val.message}`;
            if (val.params && val.params.allowedValues) {
                message = `${message}. Allowed values: '${val.params.allowedValues.join(
                    "', '",
                )}'`;
            }
            return {
                property,
                message,
                code: 'REQUEST_VALIDATION_ERROR',
            };
        });

        const err = new CustomError(400, '', '', errors);

        return err;
    }

    return null;
};

// const databaseErrorHandler: ErrorHandler<Error> = error => {
//     if (
//         error.name === 'QueryFailedError' &&
//         error.message.split(' ').includes('duplicate')
//     ) {
//         const err = UserAlreadyExists();
//         return err;
//     }

//     if (error.name === 'QueryFailedError') {
//         const err = BadRequestError();
//         return err;
//     }

//     if (error.name === 'EntityNotFoundError') {
//         const err = UserNotFound();
//         return err;
//     }

//     return null;
// };

const fastifyErrorHandler: ErrorHandler<Error> = error => {
    if (error?.message?.includes('FST_ERR_CTP_EMPTY_JSON_BODY')) {
        const err = CustomError.create(
            400,
            'INVALID_PAYLOAD_FORMAT',
            'The payload must be in valid JSON format and cannot be empty if header contains Content-Type as application/json',
        );

        return err;
    }

    if (error?.message?.includes('Unexpected token')) {
        const err = CustomError.create(
            400,
            'INVALID_JSON_FORMAT',
            'unexpected token in json',
        );

        return err;
    }

    return null;
};

const authorizationErrorHandler: ErrorHandler<Error> = (
    error,
    _,
    reply,
) => {
    if (error.name === 'UnauthorizedError') {
        const err = ForbiddenError();
        return err;
    }

    if (reply!.statusCode === 429) {
        const err = CustomError.create(
            429,
            'TOO_MANY_REQUESTS',
            'You hit the rate limit! Slow down please!',
        );
        return err;
    }

    if (reply!.statusCode === 403) {
        const err = CustomError.create(
            403,
            'BANNED_USER',
            'Please contact support!',
        );
        return err;
    }

    return null;
};

const classValidatorErrorHandler: ErrorHandler<Error> = error => {
    if (!Array.isArray(error)) return null;

    // TODO Checking if this is the error schema from Class-Validator
    if (!(error.length && error[0].property && error[0].constraints))
        return null;

    const errors = error.map((_err: any) => {
        const { property, constraints } = _err;

        // TODO Only picking the first message...
        // TODO have to check if we get more properties
        const [message] = Object.keys(constraints).map(key => {
            return constraints[key];
        });

        return CustomError.serializeError(
            'VALIDATION_ERROR',
            `Property: ${property} - ${message}`,
        );
    });

    return CustomError.create(400, '', '', errors);
};

export {
    customErrorHandler,
    validationErrorHandler,
    fastifyErrorHandler,
    authorizationErrorHandler,
    classValidatorErrorHandler,
};

import {
    FastifyRequest,
    FastifyReply,
    FastifyInstance,
} from 'fastify';
import { CustomError } from './CustomError';
import { InternalServerError } from './CoreErrors';
import { ErrorHandlers } from './types';
import { AjvError } from '.';

export default <T extends ErrorHandlers>(errorHandlers: T) =>
    async function _(
        this: FastifyInstance,
        error: CustomError & Error & AjvError,
        req: FastifyRequest,
        reply: FastifyReply,
    ): Promise<FastifyReply> {
        let errResponse:
            | (CustomError & {
                  actualError?: Error;
              })
            | undefined;

        errorHandlers.forEach(handler => {
            const err = handler(error, req, reply);
            if (err) {
                errResponse = err;
            }
        });

        if (!errResponse) {
            errResponse = InternalServerError();
            errResponse.actualError = {
                name: error.name,
                msg: error.message,
                stack: error.stack,
            };
        }

        req.log.error(
            {
                statusCode: errResponse.statusCode,
                stack: errResponse.stack,
                actualError: errResponse.actualError,
            },
            errResponse?.message,
        );

        return reply
            .code(errResponse.statusCode)
            .send(errResponse.response);
    };

export class CustomError extends Error {
    public stack: string;

    constructor(
        public statusCode: number,
        public name: string,
        public message: string,
        public errors: any[],
        public actualError?: any,
    ) {
        super(message);

        Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
    }

    get response() {
        return {
            errors: this.errors,
        };
    }

    static create(
        statusCode: number,
        name: string,
        message: string,
        errors?: any[],
    ) {
        let errs: any = errors;
        if (!errs) {
            errs = [this.serializeError(name, message)];
        }

        return new CustomError(statusCode, name, message, errs);
    }

    static serializeError(name: string, message: string) {
        return {
            code: name,
            message,
        };
    }
}

export default CustomError;

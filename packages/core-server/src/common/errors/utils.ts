// TODO legacy

export const getPropertyPath = (val: any) =>
    (val.dataPath && val.params.missingProperty
        ? `${val.dataPath}/${val.params.missingProperty}`
        : val.params.missingProperty ||
          val.dataPath ||
          val.params.propertyName ||
          val.propertyName ||
          val.params.additionalProperty ||
          'empty_property_key'
    )
        .replace('.', '')
        .replace('/', '');

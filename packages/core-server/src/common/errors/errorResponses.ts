import HTTP from 'http-status-codes';

export default {
    INVALID_CREDENTIALS_ERROR: {
        name: 'INVALID_CREDENTIALS_ERROR',
        message: 'Invalid credentials',
        httpStatusCode: HTTP.BAD_REQUEST,
    },
    FORBIDDEN_ERROR: {
        name: 'FORBIDDEN_ERROR',
        message: 'Required credentials are missing',
        httpStatusCode: HTTP.FORBIDDEN,
    },
    // PASSWORD_REUSE_ERROR: {
    //     name: 'PASSWORD_REUSE_ERROR',
    //     message: 'Please Provide a password not used before',
    //     httpStatusCode: HTTP.BAD_REQUEST,
    // },
    // PASSWORD_LOCKED_ERROR: {
    //     name: 'PASSWORD_LOCKED_ERROR',
    //     message: 'Password has been lock, please proceed with an OTP',
    //     httpStatusCode: HTTP.LOCKED,
    // },
    BAD_REQUEST_ERROR: {
        name: 'BAD_REQUEST_ERROR',
        message: 'Invalid Request',
        httpStatusCode: HTTP.BAD_REQUEST,
    },
    INTERNAL_SERVER_ERROR: {
        name: 'INTERNAL_SERVER_ERROR',
        message: 'Internal server error',
        httpStatusCode: HTTP.INTERNAL_SERVER_ERROR,
    },
    MISSING_PARAMS_ERROR: {
        name: 'MISSING_PARAMS_ERROR',
        message:
            'Required parameter is not present in request header',
        httpStatusCode: HTTP.BAD_REQUEST,
    },
    // USER_ALREADY_EXISTS: {
    //     name: 'USER_ALREADY_EXISTS',
    //     message:
    //         'Please provide a different username for registration',
    //     httpStatusCode: HTTP.CONFLICT,
    // },
    // USER_NOT_FOUND: {
    //     name: 'USER_NOT_FOUND',
    //     message: 'User does not exist',
    //     httpStatusCode: HTTP.NOT_FOUND,
    // },
    // ADDRESS_NOT_FOUND: {
    //     name: 'ADDRESS_NOT_FOUND',
    //     message: 'Address does not exist',
    //     httpStatusCode: HTTP.NOT_FOUND,
    // },
};

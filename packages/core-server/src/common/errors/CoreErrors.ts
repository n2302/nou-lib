import { CustomError } from './CustomError';
import responseErrors from './errorResponses';

const InvalidCredentials = () =>
    CustomError.create(
        responseErrors.INVALID_CREDENTIALS_ERROR.httpStatusCode,
        responseErrors.INVALID_CREDENTIALS_ERROR.name,
        responseErrors.INVALID_CREDENTIALS_ERROR.message,
    );
const ForbiddenError = () =>
    CustomError.create(
        responseErrors.FORBIDDEN_ERROR.httpStatusCode,
        responseErrors.FORBIDDEN_ERROR.name,
        responseErrors.FORBIDDEN_ERROR.message,
    );
const BadRequestError = () =>
    CustomError.create(
        responseErrors.BAD_REQUEST_ERROR.httpStatusCode,
        responseErrors.BAD_REQUEST_ERROR.name,
        responseErrors.BAD_REQUEST_ERROR.message,
    );
const InternalServerError = () =>
    CustomError.create(
        responseErrors.INTERNAL_SERVER_ERROR.httpStatusCode,
        responseErrors.INTERNAL_SERVER_ERROR.name,
        responseErrors.INTERNAL_SERVER_ERROR.message,
    );
const MissingParamsError = () =>
    CustomError.create(
        responseErrors.MISSING_PARAMS_ERROR.httpStatusCode,
        responseErrors.MISSING_PARAMS_ERROR.name,
        responseErrors.MISSING_PARAMS_ERROR.message,
    );
// const PasswordReuseError = () =>
//     CustomError.create(
//         responseErrors.PASSWORD_REUSE_ERROR.httpStatusCode,
//         responseErrors.PASSWORD_REUSE_ERROR.name,
//         responseErrors.PASSWORD_REUSE_ERROR.message,
//     );
// const PasswordLockedError = () =>
//     CustomError.create(
//         responseErrors.PASSWORD_LOCKED_ERROR.httpStatusCode,
//         responseErrors.PASSWORD_LOCKED_ERROR.name,
//         responseErrors.PASSWORD_LOCKED_ERROR.message,
//     );
// export const UserAlreadyExists = () =>
//     CustomError.create(
//         responseErrors.USER_ALREADY_EXISTS.httpStatusCode,
//         responseErrors.USER_ALREADY_EXISTS.name,
//         responseErrors.USER_ALREADY_EXISTS.message,
//     );

// export const UserNotFound = () =>
//     CustomError.create(
//         responseErrors.USER_NOT_FOUND.httpStatusCode,
//         responseErrors.USER_NOT_FOUND.name,
//         responseErrors.USER_NOT_FOUND.message,
//     );
// export const AddressNotFound = () =>
//     CustomError.create(
//         responseErrors.ADDRESS_NOT_FOUND.httpStatusCode,
//         responseErrors.ADDRESS_NOT_FOUND.name,
//         responseErrors.ADDRESS_NOT_FOUND.message,
//     );

export {
    InvalidCredentials,
    ForbiddenError,
    BadRequestError,
    InternalServerError,
    // PasswordLockedError,
    MissingParamsError,
};

import { FastifyReply, FastifyRequest } from 'fastify';
import { CustomError } from './CustomError';

export interface AjvError extends Error {
    type: string;
    validation: unknown[];
    errors: unknown[];
}

export type ErrorHandler<E extends Error> = (
    error: E,
    request?: FastifyRequest,
    reply?: FastifyReply,
) => CustomError | null;

export type ErrorHandlers = ErrorHandler<CustomError & AjvError>[];

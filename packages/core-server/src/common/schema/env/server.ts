export default {
    SERVER_PORT: {
        type: 'number',
        default: 5000,
    },
    SERVER_HOST: {
        type: 'string',
        default: '0.0.0.0',
    },
    SERVER_IS_PROD: {
        type: 'boolean',
        default: false,
    },
    SERVER_BASE_URL: {
        type: 'string',
        default: 'localhost',
    },
};

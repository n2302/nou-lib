const commonErrorSchema = {
    type: 'object',
    properties: {
        errors: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    code: { type: 'string' },
                    message: { type: 'string' },
                },
            },
        },
    },
};

export default commonErrorSchema;

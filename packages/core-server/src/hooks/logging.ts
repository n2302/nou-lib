import {
    FastifyReply,
    FastifyRequest,
    HookHandlerDoneFunction,
    FastifyInstance,
} from 'fastify';

export function onRequest(
    this: FastifyInstance,
    request: FastifyRequest,
    _: FastifyReply,
    done: HookHandlerDoneFunction,
) {
    request.log.info(
        {
            method: request.method,
            url: request.url,
            parameters: request.params,
            // Including the headers in the log could be in violation
            // of privacy laws, e.g. GDPR. You should use the "redact" option to
            // remove sensitive fields. It could also leak authentication data in
            // the logs.
            headers: request.headers,
            hostname: request.hostname,
            remoteAddress: request.ip,
            remotePort: request.socket.remotePort,
        },
        'incoming request',
    );
    done();
}

export function onResponse(
    this: FastifyInstance,
    req: FastifyRequest,
    reply: FastifyReply,
    done: HookHandlerDoneFunction,
) {
    req.log.info(
        {
            url: req.url, // add url to response as well for simple correlatingurl: req.url, // add url to response as well for simple correlating
            statusCode: reply.statusCode,
            // Including the headers in the log could be in violation
            // of privacy laws, e.g. GDPR. You should use the "redact" option to
            // remove sensitive fields. It could also leak authentication data in
            // the logs.
            headers: reply.getHeaders(),
            responseTime: reply.getResponseTime(),
            sent: reply.sent,
        },
        'request completed',
    );
    done();
}

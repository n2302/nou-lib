import { compare, hash as _hash } from 'bcrypt';
import { FastifyInstance } from 'fastify';
import fp from 'fastify-plugin';

export interface bcrypt {
    hash: (data: string | Buffer) => Promise<string>;
    compare: typeof compare;
}

declare module 'fastify' {
    interface FastifyInstance {
        bcrypt: bcrypt;
    }
}

const registerHash = (saltOrRounds?: string | number) => {
    if (!saltOrRounds)
        throw new Error(
            'saltOrRounds value must be provided for plugin',
        );
    return async (data: string | Buffer): Promise<string> => {
        return _hash(data, saltOrRounds);
    };
};

export default fp(
    async (
        fastifyInstance: FastifyInstance,
        options: {
            saltRounds: string | number;
        },
    ): Promise<void> => {
        fastifyInstance.decorate('bcrypt', {
            hash: registerHash(options.saltRounds),
            compare,
        });
    },
);

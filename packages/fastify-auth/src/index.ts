import fp from 'fastify-plugin';
import { FastifyInstance } from 'fastify';
import { JsonWebToken } from './JsonWebToken';
import type { JsonWebTokenConfig } from './types';
import 'reflect-metadata';

export { JsonWebTokenConfig } from './types';

declare module 'fastify' {
    interface FastifyInstance {
        auth: JsonWebToken;
    }
}

export default fp(
    async (
        fastifyInstance: FastifyInstance,
        options: JsonWebTokenConfig,
    ): Promise<void> => {
        fastifyInstance.decorate(
            'auth',
            await new JsonWebToken.Builder()
                .withSecret(options.secret)
                .withIssuer(options.issuer)
                .withAudience(options.audience)
                .withAlgorithm(options.algorithm)
                .withTokenExpiration(options.tokenExpiration)
                .withKeyid(options.keyid)
                .build(),
        );
    },
);

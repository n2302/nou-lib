/* eslint-disable camelcase */
import { randomUUID } from 'crypto';
import {
    IsArray,
    IsBase64,
    IsNotEmpty,
    IsNotEmptyObject,
    IsObject,
    IsString,
    validateOrReject,
} from 'class-validator';
import {
    Algorithm,
    decode,
    JwtPayload,
    sign,
    verify,
} from 'jsonwebtoken';
import {
    ACCESS_TOKEN,
    ID_TOKEN,
    REFRESH_TOKEN,
    SignOptions,
    TokenExpiration,
    TokenPayload,
    TokenType,
} from './types';
import { Token } from './Token';

export class JsonWebToken {
    @IsArray()
    @IsNotEmpty({ each: true })
    private algorithms: Algorithm[]; // TODO ['ES256']

    @IsString()
    @IsNotEmpty()
    private algorithm: Algorithm; // TODO 'ES256'

    @IsString()
    @IsNotEmpty()
    private issuer: string; // TODO https://localhost:5000 or Prod

    @IsBase64()
    @IsString()
    @IsNotEmpty()
    private keyid: string; // TODO RGVmYXVsdA== -> Default in base64

    @IsArray()
    @IsNotEmpty({ each: true })
    private audience: Array<string>; // TODO can be [api://graphql,api://authorization]

    // ... Config

    @IsObject()
    @IsNotEmptyObject()
    private tokenExpiration: TokenExpiration;

    @IsNotEmpty()
    private secret:
        | string
        | Buffer
        | { key: string | Buffer; passphrase: string };

    static get Builder() {
        return class Builder {
            public jsonWebToken: JsonWebToken;

            constructor() {
                this.jsonWebToken = new JsonWebToken();
            }

            withTokenExpiration(tokenExpiration: TokenExpiration) {
                this.jsonWebToken.tokenExpiration = tokenExpiration;
                return this;
            }

            withSecret(secret: string) {
                this.jsonWebToken.secret = secret;
                return this;
            }

            withAlgorithm(algorithm: Algorithm) {
                this.jsonWebToken.algorithm = algorithm;
                this.jsonWebToken.algorithms = [algorithm];
                return this;
            }

            withIssuer(issuer: string) {
                this.jsonWebToken.issuer = issuer;
                return this;
            }

            withAudience(audience: string[]) {
                this.jsonWebToken.audience = audience;
                return this;
            }

            withKeyid(keyid: string) {
                this.jsonWebToken.keyid = keyid;
                return this;
            }

            async build() {
                await validateOrReject(this.jsonWebToken);
                return this.jsonWebToken;
            }
        };
    }

    // eslint-disable-next-line class-methods-use-this
    async decodeToken(token: string) {
        return decode(token);
    }

    async verifyTokens(
        externalToken: string,
        internalToken: JwtPayload,
    ) {
        try {
            return verify(externalToken, this.secret, {
                issuer: this.issuer,
                algorithms: this.algorithms,
                audience: internalToken.aud,
                jwtid: internalToken.jti,
                nonce: internalToken.nonce,
                subject: internalToken.sub,
            });
        } catch (error) {
            return false;
        }
    }

    async verifyToken(externalToken: string) {
        try {
            return verify(externalToken, this.secret, {
                issuer: this.issuer,
                audience: this.audience,
                algorithms: this.algorithms,
            });
        } catch (error) {
            return false;
        }
    }

    async createToken(
        type: TokenType,
        payload: TokenPayload,
        options: SignOptions,
    ) {
        let token_payload: any;

        const expiresIn =
            this.tokenExpiration[type][payload.client_id];
        if (!expiresIn) throw new Error('expiresIn value is missing');

        switch (type) {
            case REFRESH_TOKEN:
                token_payload = await new Token.BuildRefreshToken()
                    .withClient_id(payload.client_id)
                    .withNonce(payload.nonce)
                    .build();
                break;
            case ACCESS_TOKEN:
                token_payload = await new Token.BuildAccessToken()
                    .withClient_id(payload.client_id)
                    .withNonce(payload.nonce)
                    .withScope(payload.scope)
                    .withAmr(payload.amr)
                    .build();
                break;
            case ID_TOKEN:
                token_payload = await new Token.BuildIdToken()
                    .withClient_id(payload.client_id)
                    .withNonce(payload.nonce)
                    .withEmail(payload.email)
                    .withName(payload.name)
                    .withAzp(payload.azp)
                    .withPicture(payload.picture)
                    .withAddresses(payload.addresses)
                    .withSessionInfo(payload.sessionInfo)
                    .build();
                break;
            default:
                break;
        }

        return sign(token_payload, this.secret, {
            expiresIn,
            issuer: this.issuer,
            keyid: this.keyid,
            algorithm: this.algorithm,
            subject: options.subject,
            audience: options.audience,
            jwtid: randomUUID(),
        });
    }
}

export default JsonWebToken;

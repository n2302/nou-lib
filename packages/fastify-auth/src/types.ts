/* eslint-disable camelcase */
import {
    Algorithm,
    SignOptions as _SO_,
    VerifyOptions as _VO_,
} from 'jsonwebtoken';

export interface SignOptions extends _SO_ {
    audience: string[];
    subject: string;
}

export interface VerifyOptions extends _VO_ {
    audience: string[];
    jwtid: string;
    subject: string;
    nonce?: string;
}

export const REFRESH_TOKEN = 'REFRESH_TOKEN';
export const ACCESS_TOKEN = 'ACCESS_TOKEN';
export const ID_TOKEN = 'ID_TOKEN';

export type TokenType = 'REFRESH_TOKEN' | 'ACCESS_TOKEN' | 'ID_TOKEN';

export interface TokenExpiration {
    ACCESS_TOKEN: {
        [client_id: string]: number;
    };
    ID_TOKEN: {
        [client_id: string]: number;
    };
    REFRESH_TOKEN: {
        [client_id: string]: number;
    };
}

export interface JsonWebTokenConfig {
    secret: string;
    keyid: string;
    issuer: string;
    audience: string[];
    tokenExpiration: TokenExpiration;
    algorithm: Algorithm;
}

export type AMR = 'pwd' | 'otp' | 'mf';

export interface TokenPayload {
    [key: string]: any;
    scope?: string[];
    amr?: AMR[];
    nonce?: string;
    client_id: string;
}

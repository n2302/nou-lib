/* eslint-disable camelcase */
/* eslint-disable max-classes-per-file */
import {
    validateOrReject,
    IsString,
    IsOptional,
    IsArray,
    IsNotEmpty,
    IsNotEmptyObject,
    IsObject,
} from 'class-validator';
import { Expose, instanceToPlain } from 'class-transformer';
import { ACCESS_TOKEN, AMR, ID_TOKEN, REFRESH_TOKEN } from './types';

export class Token {
    @Expose({ groups: [ACCESS_TOKEN, ID_TOKEN, REFRESH_TOKEN] })
    @IsNotEmpty()
    @IsString()
    public client_id: string;

    @Expose({ groups: [ACCESS_TOKEN, ID_TOKEN, REFRESH_TOKEN] })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public nonce?: string;

    // Access
    @Expose({ groups: [ACCESS_TOKEN] })
    @IsOptional()
    @IsNotEmpty({ each: true })
    @IsArray()
    public amr?: AMR[];

    @Expose({ groups: [ACCESS_TOKEN] })
    @IsOptional()
    @IsNotEmpty({ each: true })
    @IsArray()
    public scope?: string[];

    // Id Token
    @Expose({ groups: [ID_TOKEN] })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public picture?: string;

    @Expose({ groups: [ID_TOKEN] })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public name?: string;

    @Expose({ groups: [ID_TOKEN] })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public email?: string;

    @Expose({ groups: [ID_TOKEN] })
    @IsOptional()
    @IsNotEmpty({ each: true })
    @IsArray()
    public addresses?: string[];

    @Expose({ groups: [ID_TOKEN] })
    @IsOptional()
    @IsNotEmpty()
    public azp?: string;

    @Expose({ groups: [ID_TOKEN] })
    @IsOptional()
    @IsNotEmptyObject()
    @IsObject()
    public sessionInfo?: Record<string, any>;

    // @TransformInstanceToPlain({ groups: [ACCESS_TOKEN] })
    static get BuildAccessToken() {
        return class Builder {
            public access_token: Token;

            constructor() {
                this.access_token = new Token();
            }

            withClient_id(clientId: string) {
                this.access_token.client_id = clientId;
                return this;
            }

            withNonce(nonce?: string) {
                this.access_token.nonce = nonce;
                return this;
            }

            withScope(scope?: string[]) {
                this.access_token.scope = scope;
                return this;
            }

            withAmr(amr?: AMR[]) {
                this.access_token.amr = amr;
                return this;
            }

            async build() {
                await validateOrReject(this.access_token);
                return instanceToPlain(this.access_token, {
                    groups: [ACCESS_TOKEN],
                });
            }
        };
    }

    // @TransformInstanceToPlain({ groups: [REFRESH_TOKEN] })
    static get BuildRefreshToken() {
        return class Builder {
            public refresh_token: Token;

            constructor() {
                this.refresh_token = new Token();
            }

            withClient_id(clientId: string) {
                this.refresh_token.client_id = clientId;
                return this;
            }

            withNonce(nonce?: string) {
                this.refresh_token.nonce = nonce;
                return this;
            }

            async build() {
                await validateOrReject(this.refresh_token);
                return instanceToPlain(this.refresh_token, {
                    groups: [REFRESH_TOKEN],
                });
            }
        };
    }

    // @TransformInstanceToPlain({ groups: [ID_TOKEN] })
    static get BuildIdToken() {
        return class Builder {
            public id_token: Token;

            constructor() {
                this.id_token = new Token();
            }

            withClient_id(clientId: string) {
                this.id_token.client_id = clientId;
                return this;
            }

            withNonce(nonce?: string) {
                this.id_token.nonce = nonce;
                return this;
            }

            withPicture(picture?: string) {
                this.id_token.picture = picture;
                return this;
            }

            withName(name?: string) {
                this.id_token.name = name;
                return this;
            }

            withEmail(email?: string) {
                this.id_token.email = email;
                return this;
            }

            withAddresses(addresses?: string[]) {
                this.id_token.addresses = addresses;
                return this;
            }

            withAzp(azp?: string) {
                this.id_token.azp = azp;
                return this;
            }

            withSessionInfo(sessionInfo?: Record<string, any>) {
                this.id_token.sessionInfo = sessionInfo;
                return this;
            }

            async build() {
                await validateOrReject(this.id_token);
                return instanceToPlain(this.id_token, {
                    groups: [ID_TOKEN],
                });
            }
        };
    }
}

export default Token;

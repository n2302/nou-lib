import { FastifyInstance } from 'fastify';
import fp from 'fastify-plugin';
import swagger from 'fastify-swagger';

import { swaggerConfig, serverConfig } from './packages/core-server/src/config';

export default fp(async (fastifyInstance: FastifyInstance) => {
    try {
        fastifyInstance.register(swagger, swaggerConfig);
        fastifyInstance.log.info(
            `SWAGGER is at http://localhost:${
                serverConfig().port
            }/documentation`,
        );
    } catch (error) {
        fastifyInstance.log.error(error);
    }
});

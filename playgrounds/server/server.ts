import FastifyWrapper, { config, errors } from '@knou/core-server';

const Main = async () => {
    FastifyWrapper.preload('.env.dev');
    const Server = await FastifyWrapper.build();
    Server.server.setErrorHandler(
        errors.registerErrorHandlers([
            errors.authorizationErrorHandler,
            errors.customErrorHandler,
            errors.classValidatorErrorHandler,
            errors.validationErrorHandler,
            errors.classValidatorErrorHandler,
        ]),
    );
    await Server.ready();
    await Server.listen(config.server().port, config.server().host);
    await Server.printRoutes();
};

export default Main;

Main();
